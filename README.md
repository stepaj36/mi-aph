PixiJS boilerplate for MI-APH
=========

## How to start

* Clone content of this repository into your local folder
* Install [Node Package Manager](https://www.npmjs.com)
* Execute command `npm install` and `npm start`
* Open `localhost:1234` in your browser


## Game
* Use arrows to move, X to fire
* Try to survive as long as you can
