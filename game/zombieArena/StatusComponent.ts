import { Model } from './Model';
import { ATTR_MODEL, MSG_GAME_OVER, MSG_NEW_WAVE } from './Constants';
import { PIXICmp } from '../../ts/engine/PIXIObject';
import { GenericComponent } from '../../ts/components/GenericComponent';

/**
 * Component for displaying game status (number of wave, game over)
 */
export class StatusComponent extends GenericComponent {
    private model: Model;

    constructor() {
        // use GenericComponent to pile up all message handlers
        super(StatusComponent.name);
        this.doOnMessage(MSG_NEW_WAVE, (cmp, msg) => this.showText(`WAVE ${this.model.currentWave + 1}`));
        this.doOnMessage(MSG_GAME_OVER, (cmp, msg) => this.showText(`GAME OVER`));
    }

    onInit() {
        super.onInit();
        this.model = this.owner.getScene().getGlobalAttribute(ATTR_MODEL);
    }

    /**
    * Sets a new text and make it visible
    */
    protected showText(text: string) {
        let textObj = <PIXICmp.Text>this.owner;
        textObj.text = text;
        textObj.visible = true;

        this.scene.invokeWithDelay(1000, () => {
            textObj.visible = false;
        });
    }
}