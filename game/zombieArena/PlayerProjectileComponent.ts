import { DynamicsComponent } from "../../ts/components/DynamicsComponent";
import Msg from "../../ts/engine/Msg";
import { MSG_COLLIDABLE_DESTROYED } from "./Constants";

/**
 * Component for managing projectiles
 */
export class PlayerProjectileComponent extends DynamicsComponent {
    private remove: boolean;

    onInit() {
        super.onInit();
        this.subscribe(MSG_COLLIDABLE_DESTROYED);
        this.remove = false;
    }

    onUpdate(delta, absolute) {
        super.onUpdate(delta, absolute);
        if (this.remove) this.owner.remove(); //When remove flag is on - remove projectile from scene
    }

    onMessage(msg: Msg) {
        if (msg.data !== this.owner) return; //Filter messages for other objects

        this.remove = true;
    }
}