import Vec2 from '../../ts/utils/Vec2';
import { STORAGE_ID_BOX, STORAGE_ID_FLOOR, STORAGE_ID_SPAWNER, STORAGE_ID_PLAYER } from './Constants';
import { WorldMap } from './WorldMap';

/*
* Entity for storing information about background blocks
*/
export class Background {
    position: Vec2;
}

/*
* Attributes of player
*/
export class PlayerModel {
    speed: number = 0.11;
    projectileSpeed: number = 8;
    gunFireRate: number = 3;
    hp: number = 5;
}

/*
* Attributes of zombie
*/
export class ZombieModel {
    attackFrequency: number = 2;
    disappearFrequency: number = 0.5;
}

// number of columns of each level
export const COLUMNS_NUM = 11;

/**
 * Entity that stores metadata about animtions of each sprite as loaded from JSON file
 */
export class AnimationInfo {

    constructor(assignedState: number, begin: number, frames: number, fps: number) {
        this.assignedState = assignedState;
        this.begin = begin;
        this.frames = frames;
        this.fps = fps;
    }

    assignedState: number;
    begin: number; //Number of first frame
    frames: number;
    fps: number;
}

/**
 * Entity that stores metadata about each sprite as loaded from JSON file
 */
export class SpriteInfo {

    constructor(name: string, offsetX: number, offsetY: number, width: number, height: number, animations: Array<AnimationInfo>) {

        this.name = name;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
        this.width = width;
        this.height = height;
        this.animations = animations;
    }

    name: string;
    offsetX: number;
    offsetY: number;
    width: number;
    height: number;
    animations: Array<AnimationInfo>;
}

export class Model {

    //map
    map: WorldMap;

    backgroundBlocks: Array<Background>;
    // metadata of all sprites as loaded from JSON file
    sprites: Array<SpriteInfo>;
    // 2D arrays of levels as loaded from JSON file
    levels: Array<Array<Array<number>>>;

    spawnFrequency = 1; //1 zombie per second

    currentLevel = 0;
    numLevels: number;

    currentWave = 0;
    score = 0;
    zombiesSpawned = 0;
    numZombiesWave = 3; // Number of zombies spawned by one spawn

    //Position of player on level start
    playerInitX: number;
    playerInitY: number;

    /**
     * Loads model from a JSON structure
     */
    loadModel(data: any) {
        this.sprites = new Array<SpriteInfo>();

        for(let spr of data.sprites) {
            let animations = new Array<AnimationInfo>();
            for(let anim of spr.anims) {
                animations.push(new AnimationInfo(anim.assignedState, anim.begin, anim.frames, anim.fps));
            }
            this.sprites.push(new SpriteInfo(spr.name, spr.offset_px_x, spr.offset_px_y, spr.sprite_width, spr.sprite_height, animations));
        }

        this.levels = new Array();
        
        for(let level of data.levels_maps){
            this.levels.push(level);
        }

        this.numLevels = this.levels.length;
    }

    /**
     * Initializes the current level
     */
    initLevel() {
        this.backgroundBlocks = new Array<Background>();

        this.currentWave = 0;
        this.score = 0;
        this.zombiesSpawned = 0;
        this.numZombiesWave = 3;
        
        this.map = new WorldMap(COLUMNS_NUM, COLUMNS_NUM);

        this.loadBricks();
        this.map.init(); //Init map after loading objects from level
    }

    getSpriteInfo(name: string): SpriteInfo {
        for (let spr of this.sprites) {
            if (spr.name == name) return spr;
        }
        return null;
    }
    
    /**
     * Fills map of bricks from an array the level is represented by
     */
    protected loadBricks() {
        for (let row = 0; row < this.levels[this.currentLevel-1].length; row++) {
            for (let col = 0; col < COLUMNS_NUM; col++) {

                let objId = this.levels[this.currentLevel-1][row][col];

                switch(objId) {
                    case STORAGE_ID_PLAYER:
                    {
                        this.playerInitX = col + 0.5;
                        this.playerInitY = row + 0.5;
                    }
                    case STORAGE_ID_FLOOR:
                    {
                        let bg = new Background();
                        bg.position = new Vec2(col, row);
                        this.backgroundBlocks.push(bg);
                    }
                    break;
                    case STORAGE_ID_BOX:
                    case STORAGE_ID_SPAWNER:
                    this.map.addBlock(col, row, objId);
                    break;
                }
            }
        }
    }
}
