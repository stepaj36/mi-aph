import { DynamicsComponent } from "../../ts/components/DynamicsComponent";
import { SteeringMath } from "../../ts/utils/SteeringMath";
import Vec2 from "../../ts/utils/Vec2";

/**
 * Main controller for various steering behaviour types
 */
export abstract class SteeringComponent extends DynamicsComponent {
    math = new SteeringMath();

    protected abstract calcForce(delta: number): Vec2;

    onUpdate(delta: number, absolute: number) {

        // update dynamics and set new position
        let force = this.calcForce(delta);
        if (force == null) {
            return;
        }

        this.dynamics.aceleration = force;
        this.dynamics.aceleration = this.dynamics.aceleration.limit(0.2);
        this.dynamics.velocity = this.dynamics.velocity.limit(0.2);
        super.onUpdate(delta, absolute);

        // change rotation based on the velocity
        let desiredRotation = Math.atan2(this.dynamics.velocity.y, this.dynamics.velocity.x);
        let ownerPx = this.owner.getPixiObj();
        let currentRotation = ownerPx.rotation;

        let rotated = Math.abs(currentRotation - desiredRotation) < 0.1;
        if (!rotated) {
                let diff = desiredRotation - currentRotation;
                let toRotate;

                // turn only within the smaller angle
                if ((Math.abs(diff) > Math.PI)) {
                    if (diff > 0) toRotate = diff - 2 * Math.PI;
                    else toRotate = diff + 2 * Math.PI;
                } else {
                    toRotate = diff;
                }

                //Slowly rotate towards desired rotation
                ownerPx.rotation = currentRotation + toRotate * 0.2; 
        }
    }
}