// message keys
export const MSG_GAME_OVER = "GAME_OVER";
export const MSG_COLLIDABLE_CREATED = "COLLIDABLE_CREATED";
export const MSG_COLLIDABLE_DESTROYED = "COLLIDABLE_DESTROYED";
export const MSG_NEW_WAVE = "NEW_WAVE";
export const MSG_PLAYER_COLLISION = "PLAYER_COLLISION";
export const MSG_COLLISION = "COLLISION";
export const MSG_PLAYER_FOUND = "PLAYER_FOUND";
export const MSG_ZOMBIE_HIT = "ZOMBIE_HIT";
export const MSG_PLAYER_HIT = "PLAYER_HIT";

// alias for config file
export const DATA_JSON = "DATA_JSON";

// alias for texture
export const TEXTURE_ZOMBIE_ARENA = "zombie_arena";

// attribute keys
export const ATTR_MODEL = "MODEL";
export const ATTR_FACTORY = "FACTORY";
export const ATTR_ENTITY_MODEL = "ENTITY_MODEL";

// tags for game objects
export const TAG_BOX = "box";
export const TAG_FLOOR = "floor";
export const TAG_MISSILE = "missile";
export const TAG_PLAYER = "player";
export const TAG_ZOMBIE = "zombie";
export const TAG_STATUS = "status";
export const TAG_SPAWNER = "spawner";
export const TAG_PROJECTILE = "projectile";
export const TAG_SCORE = "score";
export const TAG_MENU_INTRO = "menu_intro";

// storage ids for game objects
export const STORAGE_ID_FLOOR = 0;
export const STORAGE_ID_BOX = 1;
export const STORAGE_ID_SPAWNER = 2;
export const STORAGE_ID_PLAYER = 3;

// Custom collision bounds for player and zombie
export const ENTITY_BOUNDS = 0.8; // 0.8 units in width and height
export const ENTITY_BOUNDS_HALF = ENTITY_BOUNDS / 2;


// height of the scene will be set to 11 units for the purpose of better calculations
export const SCENE_HEIGHT = 11;

// native height of the game canvas. If bigger, it will be resized accordingly
export const SPRITES_RESOLUTION_HEIGHT = 1408;

// native speed of the game
export const GAME_SPEED = 1;

// states of the player
export const PLAYER_STATE_IDLE = 0;
export const PLAYER_STATE_MOVE = 1;
export const PLAYER_STATE_SHOOT = 2;

// states of the zombies
export const ZOMBIE_STATE_IDLE = 0;
export const ZOMBIE_STATE_CHASE = 1;
export const ZOMBIE_STATE_MELEE = 2;
export const ZOMBIE_STATE_DEAD = 3;

// for positioning the barrel
export const GUN_OFFSET_X = 0.2;
export const GUN_OFFSET_Y = 0.7;

// positioning status texts
export const STATUS_X = 5;
export const STATUS_Y = 5;
export const STATUS_SCALE = 2;

// positioning score text
export const SCORE_X = 0.1;
export const SCORE_Y = 0.1;
export const SCORE_SCALE = 8;
export const SCORE_ZOMBIE_KILL = 10;

// Key codes
export const KEY_1 = 49;
export const KEY_2 = 50;
export const KEY_3 = 51;
export const KEY_4 = 52;
export const KEY_5 = 53;
export const KEY_6 = 54;
export const KEY_7 = 55;
export const KEY_8 = 56;
export const KEY_9 = 57;
