import { DynamicsComponent } from './../../ts/components/DynamicsComponent';
import { GameComponent } from './GameComponent';
import {
    ATTR_FACTORY, TEXTURE_ZOMBIE_ARENA, ATTR_MODEL, TAG_BOX, TAG_FLOOR, TAG_SPAWNER, TAG_PLAYER, STORAGE_ID_SPAWNER, STORAGE_ID_BOX, TAG_ZOMBIE,
    TAG_STATUS, TAG_PROJECTILE, GUN_OFFSET_X, GUN_OFFSET_Y, STATUS_Y, STATUS_X, STATUS_SCALE, ATTR_ENTITY_MODEL, TAG_SCORE, SCORE_SCALE, SCORE_Y,
    SCORE_X, TAG_MENU_INTRO
} from './Constants';
import Scene from '../../ts/engine/Scene';
import { Model, SpriteInfo, PlayerModel, ZombieModel } from './Model';
import PIXIObjectBuilder from '../../ts/engine/PIXIObjectBuilder';
import { IntroComponent } from './IntroComponent';
import Dynamics from '../../ts/utils/Dynamics';
import { ATTR_DYNAMICS } from './../../ts/engine/Constants';
import { PlayerInputController, PlayerComponent } from './PlayerComponent';
import { StatusComponent } from './StatusComponent';
import { PIXICmp } from '../../ts/engine/PIXIObject';
import DebugComponent from '../../ts/components/DebugComponent';
import { KeyInputComponent } from '../../ts/components/KeyInputComponent';
import { AnimatorController } from './AnimatorController';
import Vec2 from '../../ts/utils/Vec2';
import { ZombieComponent } from './ZombieComponent';
import { ZombieFollowPathComponent } from './ZombieFollowPath';
import { ZombieMeleeComponent } from './ZombieMeleeComponent';
import { ZombieSpawnerComponent } from './ZombieSpawnerComponent';
import { CollisionManager } from './CollisionManager';
import { CollisionResolver } from './CollisionResolver';
import { PlayerProjectileComponent } from './PlayerProjectileComponent';
import { ZombieDeadComponent } from './ZombieDeadComponent';
import { ScoreComponent } from './ScoreComponent';

export class Factory {

    static globalScale = 1;

    initializeLevel(scene: Scene, model: Model) {
        if (model.currentLevel == 0) {
            this.addIntro(scene, model);
        } else {
            model.initLevel();

            this.addBackground(scene, model);
            this.addBoxes(scene, model);
            this.addSpawners(scene, model);
            this.addPlayer(scene, model);
            this.addStatus(scene, model);
            this.addScore(scene, model);

            scene.addGlobalComponent(new GameComponent());
            scene.addGlobalComponent(new CollisionManager());
            scene.addGlobalComponent(new CollisionResolver());
        }
    }

    /*
    * Creates intro and adds it to scene
    */
    addIntro(scene: Scene, model: Model) {
        //Intro text
        let intro = new PIXICmp.Text(TAG_MENU_INTRO);
        intro.style = new PIXI.TextStyle({
            fontFamily: "Comfont",
            fill: "0xFFFFFF",
            fontSize: "0.5em"
        });
        intro.text = "Press ";
        intro.visible = true;
        //intro.width = 8;

        // stage components
        new PIXIObjectBuilder(scene)
            .scale(Factory.globalScale * 10)
            .relativePos(0.5, 0.25)
            .anchor(0.5)
            .withComponent(new IntroComponent())
            .build(intro, scene.stage)
    }

    /*
    * Creates background blocks (read from model) and inserts them to scene
    */
    addBackground(scene: Scene, model: Model) {
        let bricks = new PIXICmp.Container(TAG_FLOOR);
        scene.stage.getPixiObj().addChild(bricks);

        for (let block of model.backgroundBlocks) {
            let sprite = new PIXICmp.Sprite(TAG_FLOOR, this.createTexture(model.getSpriteInfo(TAG_FLOOR)));
            sprite.scale.set(Factory.globalScale);

            sprite.position.x = block.position.x;
            sprite.position.y = block.position.y;
            bricks.addChild(sprite);
        }
    }

    /*
    * Creates solid boxes (read from model) and inserts them to scene
    */
    addBoxes(scene: Scene, model: Model) {
        let boxes = new PIXICmp.Container(TAG_BOX);
        scene.stage.getPixiObj().addChild(boxes);
        let positions = model.map.getObjectsOfType(STORAGE_ID_BOX);

        for (let box of positions) {
            let sprite = new PIXICmp.Sprite(TAG_BOX, this.createTexture(model.getSpriteInfo(TAG_BOX)));
            sprite.scale.set(Factory.globalScale);

            sprite.position.x = box.position.x;
            sprite.position.y = box.position.y;
            boxes.addChild(sprite);
        }
    }

    /*
    * Creates zombie spawners (read from model) and inserts them to scene
    */
    addSpawners(scene: Scene, model: Model) {
        let positions = model.map.getObjectsOfType(STORAGE_ID_SPAWNER);
        let builder = new PIXIObjectBuilder(scene);

        for (let spawner of positions) {
            let sprite = new PIXICmp.Sprite(TAG_SPAWNER, this.createTexture(model.getSpriteInfo(TAG_FLOOR)));
            sprite.scale.set(Factory.globalScale);

            sprite.position.x = spawner.position.x;
            sprite.position.y = spawner.position.y;

            builder
                .withComponent(new ZombieSpawnerComponent())
                .build(sprite, scene.stage);
        }
    }

    /*
    * Creates a player and inserts him to scene
    */
    addPlayer(scene: Scene, model: Model) {
        let builder = new PIXIObjectBuilder(scene);

        // player
        builder
            .scale(Factory.globalScale)
            .anchor(0.5)
            .localPos(model.playerInitX, model.playerInitY)
            .withAttribute(ATTR_ENTITY_MODEL, new PlayerModel())
            .withComponent(new DynamicsComponent())
            .withComponent(new AnimatorController())
            .withComponent(new PlayerComponent())
            .withComponent(new PlayerInputController())
            .build(new PIXICmp.Sprite(TAG_PLAYER, this.createTexture(model.getSpriteInfo(TAG_PLAYER))), scene.stage);
    }

    /*
    * Creates a zombie and inserts it to scene
    */
    addZombie(scene: Scene, model: Model, pos: Vec2) {
        let builder = new PIXIObjectBuilder(scene);

        // zombie
        builder
            .scale(Factory.globalScale)
            .anchor(0.5)
            .localPos(pos.x, pos.y)
            .withAttribute(ATTR_ENTITY_MODEL, new ZombieModel())
            .withComponent(new DynamicsComponent())
            .withComponent(new AnimatorController())
            .withComponent(new ZombieFollowPathComponent())
            .withComponent(new ZombieMeleeComponent())
            .withComponent(new ZombieDeadComponent())
            .withComponent(new ZombieComponent())
            .build(new PIXICmp.Sprite(TAG_ZOMBIE, this.createTexture(model.getSpriteInfo(TAG_ZOMBIE))), scene.stage);
    }

    /*
    * Creates a projectile and inserts it to scene
    */
    createProjectile(player: PIXICmp.ComponentObject, playerModel: PlayerModel, model: Model) {
        let playerPx = player.getPixiObj();
        let rotation = playerPx.rotation;
        let cosR = Math.cos(rotation); // cache of cosine of rotation
        let sinR = Math.sin(rotation); // chache of sine of rotation
        let velocityX = playerModel.projectileSpeed * cosR;
        let velocityY = playerModel.projectileSpeed * sinR;
        let localPosX = playerPx.x + GUN_OFFSET_Y * cosR - GUN_OFFSET_X * sinR; // place in front of gun barrel
        let localPosY = playerPx.y + GUN_OFFSET_Y * sinR + GUN_OFFSET_X * cosR;
        let dynamics = new Dynamics();
        dynamics.velocity = new Vec2(velocityX, velocityY);

        let sprite = new PIXICmp.Sprite(TAG_PROJECTILE, this.createTexture(model.getSpriteInfo(TAG_PROJECTILE)));
        sprite.rotation = rotation;

        // create projectile and add it to scene
        new PIXIObjectBuilder(player.getScene())
            .localPos(localPosX, localPosY)
            .scale(Factory.globalScale)
            .withAttribute(ATTR_DYNAMICS, dynamics)
            .withComponent(new PlayerProjectileComponent())
            .build(sprite, player.getScene().stage);
    }

    /*
    * Creates a status object (game over texts, wave information) and inserts it to scene
    */
    addStatus(scene: Scene, model: Model) {
        let status = new PIXICmp.Text(TAG_STATUS);
        status.style = new PIXI.TextStyle({
            fontFamily: "Comfont",
            fill: "0xFFFFFF"
        });

        new PIXIObjectBuilder(scene)
            .scale(Factory.globalScale * STATUS_SCALE)
            .localPos(STATUS_X, STATUS_Y)
            .withComponent(new StatusComponent())
            .build(status, scene.stage);
    }

    /*
    * Creates a score object and inserts it to scene
    */
    addScore(scene: Scene, model: Model) {
        let scoreText = new PIXICmp.Text(TAG_SCORE);
        scoreText.style = new PIXI.TextStyle({
            fontFamily: "Comfont",
            fill: "0xFFFFFF",
            fontSize: "0.5em"
        });
        scoreText.text = "SCORE: 0";
        scoreText.visible = true;

        new PIXIObjectBuilder(scene)
            .scale(Factory.globalScale * SCORE_SCALE)
            .localPos(SCORE_X, SCORE_Y)
            .withComponent(new ScoreComponent())
            .build(scoreText, scene.stage);
    }

    /*
    * Reloads the entire level
    */
    resetGame(scene: Scene, model: Model) {
        scene.clearScene();
        scene.addGlobalAttribute(ATTR_FACTORY, this);
        scene.addGlobalAttribute(ATTR_MODEL, model);
        scene.addGlobalComponent(new KeyInputComponent());
        //scene.addGlobalComponent(new DebugComponent(document.getElementById("debugSect")));
        this.initializeLevel(scene, model);
    }

    /*
    * Loads texture given by sprite info object
    */
    private createTexture(spriteInfo: SpriteInfo, index: number = 0): PIXI.Texture {
        let texture = PIXI.Texture.fromImage(TEXTURE_ZOMBIE_ARENA);
        //texture.baseTexture.scaleMode = PIXI.SCALE_MODES.NEAREST;
        texture = texture.clone();
        texture.frame = new PIXI.Rectangle(spriteInfo.offsetX + spriteInfo.width * index, spriteInfo.offsetY, spriteInfo.width, spriteInfo.height);
        return texture;
    }
}