import Component from "../../ts/engine/Component";
import { MSG_PLAYER_HIT, ATTR_ENTITY_MODEL } from "./Constants";
import { ZombieModel } from "./Model";
import { checkTime } from "./Common";

/**
 * Component for zombie melee attack
 */
export class ZombieMeleeComponent extends Component {
    entityModel: ZombieModel;
    lastTime: number;

    onInit() {
        super.onInit();

        this.entityModel = this.owner.getAttribute(ATTR_ENTITY_MODEL);
        this.lastTime = 0;
    }

    beginAttack(absolute: number) {
        this.lastTime = absolute;
    }

    tryAttack(absolute: number) {
        if (checkTime(this.lastTime, absolute, this.entityModel.attackFrequency)) {
            this.sendMessage(MSG_PLAYER_HIT);
            this.lastTime = absolute;
        }
    }
}