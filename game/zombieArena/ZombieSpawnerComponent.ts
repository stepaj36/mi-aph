import Component from "../../ts/engine/Component";
import { checkTime } from "./Common";
import { MSG_NEW_WAVE, ATTR_MODEL, ATTR_FACTORY, MSG_COLLIDABLE_CREATED } from "./Constants";
import { Model } from "./Model";
import Msg from "../../ts/engine/Msg";
import { Factory } from "./Factory";
import Vec2 from "../../ts/utils/Vec2";

/**
 * Component for spawning new zombies
 */
export class ZombieSpawnerComponent extends Component {
    private lastSpawnTime = 0;
    private zombiesRemaining: number;
    private position: Vec2;

    private model: Model;
    private factory: Factory;

    onInit() {
        super.onInit();
        this.model = this.scene.getGlobalAttribute(ATTR_MODEL);
        this.factory = this.scene.getGlobalAttribute(ATTR_FACTORY);
        this.subscribe(MSG_NEW_WAVE);

        this.position = new Vec2(this.owner.getPixiObj().position.x + 0.5, this.owner.getPixiObj().position.y + 0.5);
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_NEW_WAVE) {
            this.zombiesRemaining = this.model.numZombiesWave;
        }
    }

    onUpdate(delta, absolute) {
        if (this.zombiesRemaining > 0 && checkTime(this.lastSpawnTime, absolute, this.model.spawnFrequency)) {
            // Update model stats
            this.model.zombiesSpawned++;
            this.zombiesRemaining--;

            this.lastSpawnTime = absolute;
            
            // Spawn a new zombie using Factory
            this.factory.addZombie(this.scene, this.model, this.position);
            this.sendMessage(MSG_COLLIDABLE_CREATED);
        }
    }
}