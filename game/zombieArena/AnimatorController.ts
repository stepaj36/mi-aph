import Component from "../../ts/engine/Component";
import { PIXICmp } from "../../ts/engine/PIXIObject";
import { SpriteInfo, Model, AnimationInfo } from "./Model";
import { ATTR_MODEL} from "./Constants";
import { checkTime } from "./Common";

/**
* Component that handles animation frame switching based on entity state
*/
export class AnimatorController extends Component {
    lastSwitchTime = 0;
    texture: PIXI.Texture;
    spriteInfo: SpriteInfo;
    prevState: number;

    changeAnim: boolean; //Determines if we need to change animation in next frame


    onInit() {
        super.onInit();
        this.texture = (<PIXICmp.Sprite>this.owner.getPixiObj()).texture;

        let model = <Model>this.scene.getGlobalAttribute(ATTR_MODEL);

        //Get metadata for sprite with given tag
        this.spriteInfo = model.getSpriteInfo(this.owner.getTag());

        // no animation
        this.texture.frame = new PIXI.Rectangle(this.spriteInfo.offsetX, this.spriteInfo.offsetY, this.spriteInfo.width, this.spriteInfo.height);
        this.prevState = this.owner.getState();
    }

    onUpdate(delta: number, absolute: number) {
        let anim = this.findCurrentAnim();
        let currentFrameX: number;
        
        
        if (anim == undefined) {
            // no animation
            this.texture.frame = new PIXI.Rectangle(this.spriteInfo.offsetX, this.spriteInfo.offsetY, this.spriteInfo.width, this.spriteInfo.height);
            return;
        }

        // State has changed - we should set new frame
        if (this.owner.getState() != this.prevState) {
            currentFrameX = anim.begin; //New frame is current animation first frame
            this.switchFrame(currentFrameX);

        }

        if (checkTime(this.lastSwitchTime, absolute, anim.fps)) { //Check refresh rate
            currentFrameX = this.texture.frame.x / this.spriteInfo.width; //determine current frame from offset
            
            currentFrameX++;

            if (currentFrameX >= anim.begin + anim.frames) { //Current frame is out of bounds - set back to begin
                currentFrameX = anim.begin;
            }

            if (currentFrameX == 0) { //TODO: Figure out weird anim clip while currentFrameX == 0
                currentFrameX = 1;
            }
            
            // switch animation frame
            this.switchFrame(currentFrameX);
            
            this.lastSwitchTime = absolute;
        }

        this.prevState = this.owner.getState(); //Remember previous state for future use
    }

    /*
    * Sets and offset for actual frame
    */
    private switchFrame(currentFrameX: number) {
        this.texture.frame = new PIXI.Rectangle(this.spriteInfo.width * currentFrameX, this.spriteInfo.offsetY, 
            this.spriteInfo.width, this.spriteInfo.height);
    }

    /*
    * Find animation for entity state
    */
    private findCurrentAnim(): AnimationInfo {
        for (let anim of this.spriteInfo.animations) {
            if (anim.assignedState == this.owner.getState()) return anim;
        }
    }
}