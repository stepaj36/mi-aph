import Component from "../../ts/engine/Component";
import { ZOMBIE_STATE_IDLE, TAG_PLAYER, ZOMBIE_STATE_CHASE, ZOMBIE_STATE_DEAD, ZOMBIE_STATE_MELEE, MSG_PLAYER_FOUND, 
    MSG_ZOMBIE_HIT } from "./Constants";
import { ZombieFollowPathComponent } from "./ZombieFollowPath";
import Vec2 from "../../ts/utils/Vec2";
import { ZombieMeleeComponent } from "./ZombieMeleeComponent";
import Msg from "../../ts/engine/Msg";
import { ZombieDeadComponent } from "./ZombieDeadComponent";


/**
 * Main controller for zombie
 */
export class ZombieComponent extends Component {
    private follow: ZombieFollowPathComponent;
    private melee: ZombieMeleeComponent;
    private dead: ZombieDeadComponent;

    private prevState: number;

    private lastPlayerPos: Vec2; //Last position where player was seen
    private canAttack: boolean; // Determine if zombie can attack - by collision with player
    private isShot: boolean; // Determine if zombie was shot - to set DEAD state

    onInit() {
        super.onInit();
        this.follow = <ZombieFollowPathComponent>this.owner.findComponentByClass(ZombieFollowPathComponent.name);
        this.melee = <ZombieMeleeComponent>this.owner.findComponentByClass(ZombieMeleeComponent.name);
        this.dead = <ZombieDeadComponent>this.owner.findComponentByClass(ZombieDeadComponent.name);

        this.owner.setState(ZOMBIE_STATE_IDLE);
        this.prevState = ZOMBIE_STATE_IDLE;

        this.lastPlayerPos = new Vec2(0, 0);

        this.subscribe(MSG_PLAYER_FOUND, MSG_ZOMBIE_HIT);
        this.canAttack = false;
        this.isShot = false;
    }

    onMessage(msg: Msg) {
        if (msg.data !== this.owner) return; //Filter messages for other objects

        switch (msg.action) {
            case MSG_PLAYER_FOUND:
                this.canAttack = true;
                break;
            case MSG_ZOMBIE_HIT:
                this.isShot = true;
                break;
        }
    }

    onUpdate(delta: number, absolute: number) {
        let newState = this.owner.getState();
        let stateChanged = newState != this.prevState;

        switch (this.owner.getState()) {
            case ZOMBIE_STATE_IDLE:
                newState = this.idleBeahavior();
                break;
            case ZOMBIE_STATE_CHASE:
                newState = this.chaseBeahavior();
                break;
            case ZOMBIE_STATE_MELEE:
                newState = this.meleeBehavior(stateChanged, absolute);
                break;
            case ZOMBIE_STATE_DEAD:
                newState = this.deadBehavior(stateChanged, absolute);
                break;
        }

        this.prevState = this.owner.getState();
        this.owner.setState(newState);
    }

    /*
    * Behavior in idle state
    */
    private idleBeahavior(): number {
        return ZOMBIE_STATE_CHASE;
    }

    /*
    * Behavior in chase state
    */
    private chaseBeahavior(): number {
        if (this.isShot) {
            return ZOMBIE_STATE_DEAD;
        } else if (this.canAttack) {
            return ZOMBIE_STATE_MELEE;
        }

        let player = this.scene.findFirstObjectByTag(TAG_PLAYER);
        let pos = player.getPixiObj().position;
        let playerPos = new Vec2(Math.floor(pos.x), Math.floor(pos.y));

        if (!this.lastPlayerPos.equals(playerPos)) { //Player has moved - find path
            this.lastPlayerPos = playerPos
            this.follow.goToPoint(playerPos);
        }

        return ZOMBIE_STATE_CHASE;
    }

    /*
    * Behavior in melee attacking state
    */
    private meleeBehavior(enterState: boolean, absolute: number): number {
        if (this.isShot) { //Zombie is shot - die
            return ZOMBIE_STATE_DEAD;
        } else if (this.canAttack) { //Zombie is close to a player - it can attack
            this.follow.stopFollow(); //Stop following - sets velocity to 0
            
            if (enterState) this.melee.beginAttack(absolute);
            else this.melee.tryAttack(absolute);
            
            this.canAttack = false; //Reset the flag to determine if player fled away
            return ZOMBIE_STATE_MELEE;
        } else {
            return ZOMBIE_STATE_CHASE;
        }
    }

    /*
    * Behavior in dead state
    */
    private deadBehavior(enterState: boolean, absolute: number): number {
        if (enterState) {
            this.follow.stopFollow(); //Stop following - sets velocity to 0
            this.dead.beginDeath(absolute);
        } else {
            this.dead.tryDestroy(absolute);
        }
        
        return ZOMBIE_STATE_DEAD;
    }
}