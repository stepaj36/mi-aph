import { ATTR_MODEL, ATTR_FACTORY, KEY_1, KEY_2 } from './Constants';
import { Factory } from './Factory';
import Component from '../../ts/engine/Component';
import { Model } from './Model';
import { PIXICmp } from '../../ts/engine/PIXIObject';
import { KeyInputComponent } from '../../ts/components/KeyInputComponent';

/**
 * Component that display an intro scene and jumps to the first level
 */
export class IntroComponent extends Component {
    private model: Model;
    private factory: Factory;

    onInit() {
        super.onInit();
        this.model = this.scene.getGlobalAttribute(ATTR_MODEL);
        this.factory = this.scene.getGlobalAttribute(ATTR_FACTORY);
       
        let introText = <PIXICmp.Text>this.owner;
        introText.text = "Press a level number to start a game.";

        // List all available levels
        for (let i=0 ; i < this.model.numLevels; i++) {
            introText.text += "\nLEVEL " + (i+1).toString();
        }

        introText.visible = true;
    }

    onUpdate() {
        let keyComponent = <KeyInputComponent>this.scene.stage.findComponentByClass(KeyInputComponent.name);

        // Poll key press and set up new level according to key press
        for (let i=0 ; i < this.model.numLevels; i++) {

            if (keyComponent.isKeyPressed(KEY_1 + i)) {
                this.model.currentLevel = i + 1;
                this.factory.resetGame(this.scene, this.model);
            }
        }
    }
}