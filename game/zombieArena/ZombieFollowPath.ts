import { Path, PathContext } from "../../ts/utils/Path";
import Vec2 from "../../ts/utils/Vec2";
import { SteeringComponent } from "./SteeringComponent"
import { ATTR_MODEL, ATTR_ENTITY_MODEL, ZOMBIE_STATE_CHASE } from "./Constants";
import { Model, ZombieModel } from "./Model";


/**
 * Component for Follow
 */
class FollowSteering extends SteeringComponent {

    path: Path;
    context: PathContext;
    pointTolerance = 20;
    finalPointTolerance = 3;
    maxVelocity = 8;
    slowingRadius = 30;
    pathFinished = false;

    constructor(path: Path, pointTolerance: number, finalPointTolerance: number, maxVelocity: number, slowingRadius: number) {
        super(1);
        this.path = path;
        this.pointTolerance = pointTolerance;
        this.finalPointTolerance = finalPointTolerance;
        this.maxVelocity = maxVelocity;
        this.slowingRadius = slowingRadius;
        this.context = new PathContext();
    }

    /*
    * Calculates force given by follow steering behavior
    */
    protected calcForce(delta: number): Vec2 {
        let ownerPos = new Vec2(this.owner.getPixiObj().position.x, this.owner.getPixiObj().position.y);
        let result = this.math.follow(ownerPos, this.dynamics.velocity, this.path, this.context, this.pointTolerance,
            this.finalPointTolerance, this.maxVelocity, this.slowingRadius);

        this.pathFinished = result == null;
        return result;
    }

    /*
    * Sets a new path to follow
    */
    protected resetPath(path: Path) {
        //Stop zombie to prevent it going outside path
        this.dynamics.aceleration = new Vec2(0,0);
        this.dynamics.velocity = new Vec2(0,0);

        this.pathFinished = false;
        this.path = path;
        this.context.currentPointIndex = -1;
    }
}

/**
 * Zombie component for follow steering behaviour
 */
export class ZombieFollowPathComponent extends FollowSteering {
    constructor() {
        super(new Path(), 0.8, 0.5, 1, 0.5);
    }

    model: Model;
    entityModel: ZombieModel;

    onInit() {
        this.model = this.scene.getGlobalAttribute(ATTR_MODEL);
        this.entityModel = this.owner.getAttribute(ATTR_ENTITY_MODEL);
        super.onInit();
    }

    onUpdate(delta: number, absolute: number) {
        if (!this.pathFinished && this.path.segments.length > 2) {
            super.onUpdate(delta, absolute);
        }
    }

    /*
    * Stop following path
    */
    stopFollow() {
        this.pathFinished = true;
        this.dynamics.velocity = new Vec2(0,0);
        this.dynamics.aceleration = new Vec2(0,0);
    }

    /*
    * Send zombie to given point
    */
    goToPoint(goal: Vec2) {
        let foundpath = new Array<Vec2>();

        // 1) find path from actual position to goal
        let pos = this.owner.getPixiObj().position;
        let startPos = new Vec2(Math.floor(pos.x), Math.floor(pos.y));
        if (startPos.equals(goal)) return;
        foundpath = this.model.map.findPath(startPos, goal);


        // 2) add segments into followPath object
        this.path.addFirstSegment(foundpath[0], foundpath[1]);

        for (let i = 2; i < foundpath.length; i++) {
            this.path.addSegment(foundpath[i]);
        }

        // 3) start followBehavior
        this.resetPath(this.path);
    }
}