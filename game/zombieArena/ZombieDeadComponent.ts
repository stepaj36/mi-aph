import Component from "../../ts/engine/Component";
import { ATTR_ENTITY_MODEL } from "./Constants";
import { ZombieModel } from "./Model";
import { checkTime } from "./Common";

/**
 * Component for zombie death animations, and disposal
 */
export class ZombieDeadComponent extends Component {
    entityModel: ZombieModel;
    lastTime: number;

    onInit() {
        super.onInit();

        this.entityModel = this.owner.getAttribute(ATTR_ENTITY_MODEL);
        this.lastTime = 0;
    }

    beginDeath(absolute: number) {
        this.lastTime = absolute;
    }

    tryDestroy(absolute: number) {
        if (checkTime(this.lastTime, absolute, this.entityModel.disappearFrequency)) {
            this.lastTime = absolute;
            this.owner.remove();
        }
    }
}