import { PixiRunner } from '../../ts/PixiRunner'
import { DATA_JSON, SCENE_HEIGHT, SPRITES_RESOLUTION_HEIGHT } from './Constants';
import { TEXTURE_ZOMBIE_ARENA } from './Constants';
import { Factory } from './Factory';
import { Model } from './Model';


/*
* Entry point for the whole game
*/
class ZombieArena {
    engine: PixiRunner;

    // Start a new game
    constructor() {
        this.engine = new PixiRunner();
        
        let canvas = (document.getElementById("gameCanvas") as HTMLCanvasElement);

        let screenHeight = canvas.height;
        
        // calculate ratio between intended resolution (here 400px of height) and real resolution
        // - this will set appropriate scale 
        let gameScale = SPRITES_RESOLUTION_HEIGHT / screenHeight;
        // scale the scene to 25 units if height
        let resolution = screenHeight / SCENE_HEIGHT * gameScale;
        this.engine.init(canvas, resolution / gameScale);

        // set global scale which has to be applied for ALL sprites as it will
        // scale them to defined unit size
        Factory.globalScale = 1 / resolution;

        PIXI.loader
            .reset()    // necessary for hot reload
            .add(TEXTURE_ZOMBIE_ARENA, 'static/spritesheet.png')
            .add(DATA_JSON, 'static/data.json')
            .load(() => this.onAssetsLoaded());
    }

    onAssetsLoaded() {
        // init factory and model
        let factory = new Factory();
        let model = new Model();
        model.loadModel(PIXI.loader.resources[DATA_JSON].data);
        factory.resetGame(this.engine.scene, model);
    }
}

export var zombiearena = new ZombieArena();

