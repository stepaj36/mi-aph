import { KeyInputComponent, KEY_RIGHT, KEY_DOWN, KEY_X } from '../../ts/components/KeyInputComponent';
import { ATTR_MODEL, ATTR_ENTITY_MODEL, PLAYER_STATE_MOVE, PLAYER_STATE_IDLE, MSG_PLAYER_COLLISION, MSG_PLAYER_HIT, MSG_GAME_OVER, ATTR_FACTORY, 
    MSG_COLLIDABLE_CREATED, PLAYER_STATE_SHOOT } from './Constants';
import { Model, PlayerModel } from './Model';
import Component from '../../ts/engine/Component';
import Dynamics from '../../ts/utils/Dynamics';
import { ATTR_DYNAMICS } from '../../ts/engine/Constants';

import { KEY_LEFT, KEY_UP } from '../../ts/components/KeyInputComponent';
import Vec2 from '../../ts/utils/Vec2';
import Msg from '../../ts/engine/Msg';
import { checkTime } from './Common';
import { Factory } from './Factory';

/**
 * Main controller for player
 */
export class PlayerComponent extends Component {
    private entityModel: PlayerModel;
    private model: Model;
    private dynamics: Dynamics
    private factory: Factory;

    private lastShot: number; // time when player last shot
    private gameOver: boolean;

    onInit() {
        super.onInit();
        this.entityModel = this.owner.getAttribute(ATTR_ENTITY_MODEL);
        this.dynamics = this.owner.getAttribute<Dynamics>(ATTR_DYNAMICS);
        this.model = this.scene.getGlobalAttribute(ATTR_MODEL);
        this.factory = this.scene.getGlobalAttribute(ATTR_FACTORY);

        this.owner.setState(PLAYER_STATE_IDLE);
        this.lastShot = 0;
        this.gameOver = false;

        this.subscribe(MSG_PLAYER_COLLISION, MSG_PLAYER_HIT);
    }

    onMessage(msg: Msg) {
        switch (msg.action) {
            case MSG_PLAYER_COLLISION:
                this.stopMovement();
            break;

            case MSG_PLAYER_HIT:
                this.entityModel.hp--;
                if (this.entityModel.hp <= 0) {
                    this.gameOver = true;
                    this.sendMessage(MSG_GAME_OVER);
                    this.stopMovement();
                }
            break;
        }
    }

    onUpdate(delta: number, absolute: number) {
        if (this.gameOver) return;

        this.dynamics.aceleration = this.dynamics.aceleration.limit(this.entityModel.speed * delta);

        // change rotation based on the velocity
        if (this.dynamics.velocity.magnitudeSquared() > 0) {
            let desiredRotation = Math.atan2(this.dynamics.velocity.y, this.dynamics.velocity.x);
            let ownerPx = this.owner.getPixiObj();
            let currentRotation = ownerPx.rotation;

            let rotated = Math.abs(currentRotation - desiredRotation) < 0.1; //Determine if player is in desired rotation
            if (!rotated) {
                let diff = desiredRotation - currentRotation;
                let toRotate;

                // turn only within the smaller angle
                if ((Math.abs(diff) > Math.PI)) {
                    if (diff > 0) toRotate = diff - 2 * Math.PI;
                    else toRotate = diff + 2 * Math.PI;
                } else {
                    toRotate = diff;
                }

                ownerPx.rotation = currentRotation + toRotate * 0.2; //Slowly rotate towards desired rotation
            }

            this.owner.setState(PLAYER_STATE_MOVE); // Set state to move
        } else {
            this.owner.setState(PLAYER_STATE_IDLE); // When not moving set state to idle
        }
    }

    /**
    * Moves player on given axis
    */
    move(axis: Vec2, delta: number) {
        let force = new Vec2(0, 0);

        if (this.gameOver) return;

        //Set up movement force according to moved axis
        if (axis.x > 0) {
            force.x = this.entityModel.speed * delta;
        } else if (axis.x < 0) {
            force.x = -this.entityModel.speed * delta;
        }

        if (axis.y > 0) {
            force.y = -this.entityModel.speed * delta;
        } else if (axis.y < 0) {
            force.y = this.entityModel.speed * delta;
        }

        // Move player by applying force
        this.dynamics.velocity = force;
    }

    /**
    * Shoot from gun
    */
    shoot(absolute: number) {
        this.owner.setState(PLAYER_STATE_SHOOT);
        if (checkTime(this.lastShot, absolute, this.entityModel.gunFireRate)) {
            this.lastShot = absolute;
            this.factory.createProjectile(this.owner, this.entityModel, this.model);
            this.sendMessage(MSG_COLLIDABLE_CREATED);
        }
    }

    /**
    * Stops player movenet
    */
    private stopMovement() {
        this.dynamics.aceleration = new Vec2(0,0);
        this.dynamics.velocity = new Vec2(0,0);
    }
}

/**
 * Keyboard controller for paddle
 */
export class PlayerInputController extends Component {
    private player: PlayerComponent;

    onInit() {
        // Use direct call for better response
        this.player = <PlayerComponent>this.owner.findComponentByClass(PlayerComponent.name);
    }

    onUpdate(delta: number, absolute: number) {
        let keyComponent = <KeyInputComponent>this.scene.stage.findComponentByClass(KeyInputComponent.name);
        let movement = new Vec2(0, 0);

        if (keyComponent.isKeyPressed(KEY_LEFT)) {
            movement.x = -1;
        } else if (keyComponent.isKeyPressed(KEY_RIGHT)) {
            movement.x = 1;
        }

        if (keyComponent.isKeyPressed(KEY_UP)) {
            movement.y = 1;
        } else if (keyComponent.isKeyPressed(KEY_DOWN)) {
            movement.y = -1;
        }

        if (keyComponent.isKeyPressed(KEY_X)) {
            this.player.shoot(absolute);
        }
        this.player.move(movement, delta);
    }
}