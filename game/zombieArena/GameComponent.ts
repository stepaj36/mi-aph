import { Factory } from './Factory';
import Component from '../../ts/engine/Component';
import { Model } from './Model';
import { ATTR_FACTORY, ATTR_MODEL, MSG_GAME_OVER, MSG_NEW_WAVE, MSG_ZOMBIE_HIT} from './Constants';
import Msg from '../../ts/engine/Msg';


/**
 * Component that orchestrates main logic of the game
 */
export class GameComponent extends Component {
    private model: Model;
    private factory: Factory;

    onInit() {
        this.subscribe(MSG_GAME_OVER, MSG_ZOMBIE_HIT);

        this.factory = this.scene.getGlobalAttribute(ATTR_FACTORY);
        this.model = this.scene.getGlobalAttribute(ATTR_MODEL);

        this.sendMessage(MSG_NEW_WAVE);
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_GAME_OVER) {
            this.gameOver();
        } else if (msg.action == MSG_ZOMBIE_HIT) {
            if (this.model.zombiesSpawned == 0) {
                this.setNewWave();
            }
        }
    }

    protected gameOver() {
        this.model.currentWave = 0;
        this.scene.invokeWithDelay(3000, () => this.factory.resetGame(this.scene, this.model));
    }

    /*
    * Sends a new wave of zombies after they are all dead
    */
    protected setNewWave() {
        // ceil of n * 10 % more zombies each wave
        this.model.currentWave++;
        let tmp = this.model.currentWave * 1.1; 
        let additionalZombies = Math.ceil(this.model.numZombiesWave * tmp);

        this.model.numZombiesWave += additionalZombies;

        this.sendMessage(MSG_NEW_WAVE);
    }
}