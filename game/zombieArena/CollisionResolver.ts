import Component from "../../ts/engine/Component";
import { MSG_COLLISION, TAG_PLAYER, TAG_ZOMBIE, TAG_PROJECTILE, MSG_PLAYER_FOUND, ATTR_MODEL, MSG_ZOMBIE_HIT, 
    MSG_COLLIDABLE_DESTROYED, ZOMBIE_STATE_DEAD, SCORE_ZOMBIE_KILL } from "./Constants";
import Msg from "../../ts/engine/Msg";
import { CollisionInfo } from "./CollisionManager";
import { PIXICmp } from "../../ts/engine/PIXIObject";
import { Model } from "./Model";

/**
 * Collision resolver
 */
export class CollisionResolver extends Component {
    model: Model;

    onInit() {
        super.onInit();

        this.model = this.scene.getGlobalAttribute(ATTR_MODEL);
        this.subscribe(MSG_COLLISION);
    }

    onMessage(msg: Msg) {
        //When message about collision arrive - resolve given collision
        if (msg.action == MSG_COLLISION) {
            let collisionInfo = <CollisionInfo>msg.data;
            this.resolveCollision(collisionInfo.a, collisionInfo.b)
        }
    }

    private resolveCollision(first: PIXICmp.ComponentObject, second: PIXICmp.ComponentObject) {
        //Find out what there two object represent and the decide what to do
        switch(first.getTag()) {
            case TAG_PLAYER:
            {
                if (second.getTag() == TAG_ZOMBIE) {
                    this.solvePlayerZombie(first, second);
                }
            }
            break;
            case TAG_ZOMBIE:
            {
                switch(second.getTag()) {
                    case TAG_PLAYER:
                    this.solvePlayerZombie(second, first);
                    break;
                    case TAG_PROJECTILE:
                    this.solveProjectileZombie(second, first);
                    break;
                }
            }
            break;
            case TAG_PROJECTILE:
            {
                if (second.getTag()) {
                    this.solveProjectileZombie(first, second);
                }
            }
            break;
        }
    }

    /**
    * Solve collision between player and zombie
    */
    private solvePlayerZombie(player: PIXICmp.ComponentObject, zombie: PIXICmp.ComponentObject) {
        this.sendMessage(MSG_PLAYER_FOUND, zombie);
    }

    /**
    * Solve collision between projectile and zombie
    */
    private solveProjectileZombie(projectile: PIXICmp.ComponentObject, zombie: PIXICmp.ComponentObject) {
        //Update game stats
        this.model.zombiesSpawned--;
        this.model.score += SCORE_ZOMBIE_KILL;

        this.sendMessage(MSG_ZOMBIE_HIT, zombie);
        this.sendMessage(MSG_COLLIDABLE_DESTROYED, projectile);
    }

}