import { Model } from './Model';
import { ATTR_MODEL, MSG_ZOMBIE_HIT } from './Constants';
import { PIXICmp } from '../../ts/engine/PIXIObject';
import { GenericComponent } from '../../ts/components/GenericComponent';

/**
 * Component for displaying score
 */
export class ScoreComponent extends GenericComponent {
    private model: Model;

    constructor() {
        // use GenericComponent to pile up all message handlers
        super(ScoreComponent.name);
        this.doOnMessage(MSG_ZOMBIE_HIT, (cmp, msg) => this.updateScore());
    }

    onInit() {
        super.onInit();
        this.model = this.owner.getScene().getGlobalAttribute(ATTR_MODEL);
    }

    /**
    * Updates score according to model
    */
    private updateScore() {
        let textObj = <PIXICmp.Text>this.owner;
        textObj.text = "SCORE: " + this.model.score.toString();
    }
}