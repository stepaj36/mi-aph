import { GridMap, MAP_TYPE_TILE } from '../../ts/utils/GridMap';
import Vec2 from '../../ts/utils/Vec2';
import { AStarSearch, PathFinderContext } from '../../ts/utils/Pathfinding';
import { STORAGE_ID_SPAWNER } from './Constants';

/*
* Entity for storing indestructible objects (boxes, zombie spawn)
*/
export class SolidObject {
    position: Vec2;
    type: number;

    public constructor(x: number, y: number, type: number) {
        this.position = new Vec2(x, y);
        this.type = type;
    }
}

export class WorldMap {
    // All solid objects (collidable), mapped by their indices
    objects: Map<number, SolidObject>;

    // Size of the map
    width: number;
    height: number;

    // Used for pathfinding
    gridMap: GridMap;
    astar = new AStarSearch();

    constructor(width: number, height: number) {
        this.objects = new Map<number, SolidObject>();
        this.width = width;
        this.height = height;

        // init grid map
        this.gridMap = new GridMap(MAP_TYPE_TILE, 10, this.width, this.height);
    }

    init() {
        for (let [key, obj] of this.objects) {
            if (obj.type == STORAGE_ID_SPAWNER) continue;
            this.gridMap.addObstruction(obj.position);
        }
    }

    /*
    * Adds sollid block of given type on given position
    */
    addBlock(x: number, y: number, type: number) {
        this.objects.set(y * this.width + x, new SolidObject(x, y, type));
    }

    /*
    * Determines if block on given position is solid
    */
    isSolid(x: number, y: number): boolean {
        return this.objects.has(y * this.width + x);
    }

    /*
    * Finds all solid objects of given type
    */
    getObjectsOfType(type: number): Array<SolidObject> {
        let array = new Array<SolidObject>();
        for (let [key, bl] of this.objects) {
            if (bl.type == type) {
                array.push(bl);
            }
        }
        return array;
    }

    /*
    * Returns an array with path from start to goal
    */
    findPath(from: Vec2, to: Vec2): Array<Vec2> {
        let ctx = new PathFinderContext();
        this.astar.search(this.gridMap, from, to, ctx, this.gridMap.indexMapper);
        return ctx.pathFound;
    }

}