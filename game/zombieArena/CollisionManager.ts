import Component from "../../ts/engine/Component";
import * as PIXI from 'pixi.js';
import { PIXICmp } from "../../ts/engine/PIXIObject";
import { ATTR_DYNAMICS, } from '../../ts/engine/Constants';
import Msg from '../../ts/engine/Msg';
import { MSG_COLLIDABLE_CREATED, MSG_COLLIDABLE_DESTROYED, ATTR_MODEL, TAG_ZOMBIE, TAG_PLAYER, MSG_PLAYER_COLLISION, ENTITY_BOUNDS, MSG_COLLISION, 
    ENTITY_BOUNDS_HALF, 
    ZOMBIE_STATE_DEAD} from './Constants';
import { Model } from "./Model";
import { TAG_PROJECTILE } from "./Constants"
import Dynamics from "../../ts/utils/Dynamics";
import Vec2 from "../../ts/utils/Vec2";

/**
 * Information about detected collision - handled by collision resolver
 */
export class CollisionInfo {
    a: PIXICmp.ComponentObject; //First object in collison
    b: PIXICmp.ComponentObject; //Second object

    constructor(a: PIXICmp.ComponentObject, b: PIXICmp.ComponentObject) {
        this.a = a;
        this.b = b;
    }
}

/**
 * Collision manager
 */
export class CollisionManager extends Component {
    model: Model;

    player: PIXICmp.ComponentObject;
    playerDynamics: Dynamics;

    //Collections of collidables
    zombies: Array<PIXICmp.ComponentObject>;
    projectiles = new Array<PIXICmp.ComponentObject>();

    onInit() {
        super.onInit();

        this.model = this.scene.getGlobalAttribute(ATTR_MODEL);
        this.player = this.scene.findFirstObjectByTag(TAG_PLAYER);
        this.playerDynamics = this.player.getAttribute<Dynamics>(ATTR_DYNAMICS);

        this.projectiles = this.scene.findAllObjectsByTag(TAG_PROJECTILE)
        this.zombies = this.scene.findAllObjectsByTag(TAG_ZOMBIE);

        this.subscribe(MSG_COLLIDABLE_CREATED, MSG_COLLIDABLE_DESTROYED);
    }

    onMessage(msg: Msg) {
        if (msg.action == MSG_COLLIDABLE_CREATED || msg.action == MSG_COLLIDABLE_DESTROYED) {
            // renew collections of collidable objects
            this.projectiles = this.scene.findAllObjectsByTag(TAG_PROJECTILE)
            this.zombies = this.scene.findAllObjectsByTag(TAG_ZOMBIE);
        }
    }

    onUpdate(delta, absolute) {
        let collisions = new Array<CollisionInfo>();

        //Check player collisions
        this.checkEnvironmentCollision(delta);

        //Check collisions of projectiles with environment
        this.checkProjectilesCollision();

        //Simple collision detection - redundant passes, but we should not have as much entities anyhow
        for (let zombie of this.zombies) {
            if (zombie.getState() == ZOMBIE_STATE_DEAD) continue; //Skip dead zombies
            let detected: boolean;

            detected = this.playerZombieCollision(this.player, zombie);
            if (detected) { //Player-zombie collision detected
                collisions.push(new CollisionInfo(zombie, this.player));
            }

            for (let projectile of this.projectiles) {
                detected = this.projectileZombieCollision(projectile, zombie);

                if (detected) { //Projectile-zombie collision detected
                    collisions.push(new CollisionInfo(zombie, projectile));
                }
            }
        }

        // send message for collision handling
        for (let coll of collisions) {
            this.sendMessage(MSG_COLLISION, coll);
        }
    }

    /**
    * Checks collison of player and zombie
    */
    private playerZombieCollision(player: PIXICmp.ComponentObject, zombie: PIXICmp.ComponentObject): boolean {
        let playerPosX = player.getPixiObj().x;
        let playerPosY = player.getPixiObj().y;
        let zombiePosX = zombie.getPixiObj().x;
        let zombiePosY = zombie.getPixiObj().y;

        //horizontal instersection
        let horIntersect =  Math.min(playerPosX + ENTITY_BOUNDS_HALF, zombiePosX + ENTITY_BOUNDS_HALF) - 
                            Math.max(playerPosX - ENTITY_BOUNDS_HALF, zombiePosX - ENTITY_BOUNDS_HALF);

        //vertical instersection
        let vertIntersect = Math.min(playerPosY + ENTITY_BOUNDS_HALF, zombiePosY + ENTITY_BOUNDS_HALF) - 
                            Math.max(playerPosY - ENTITY_BOUNDS_HALF, zombiePosY - ENTITY_BOUNDS_HALF);

        return horIntersect > 0 && vertIntersect > 0;
    }

    /**
    * Checks collison of projectile and zombie.
    * Projectile is small enough that we can approximate it as single point.
    */
    private projectileZombieCollision(projectile: PIXICmp.ComponentObject, zombie: PIXICmp.ComponentObject): boolean {
        let projPosX = projectile.getPixiObj().x;
        let projPosY = projectile.getPixiObj().y;
        let zombiePosX = zombie.getPixiObj().x;
        let zombiePosY = zombie.getPixiObj().y;

        return projPosX >= zombiePosX - ENTITY_BOUNDS_HALF && projPosX <= zombiePosX + ENTITY_BOUNDS_HALF &&
               projPosY >= zombiePosY - ENTITY_BOUNDS_HALF && projPosY <= zombiePosY + ENTITY_BOUNDS_HALF;
    }

    /**
    * Checks collison of player with static environment (walls, etc.)
    */
    private checkEnvironmentCollision(deltaTime: number) {
        let change = this.playerDynamics.calcPositionChange(deltaTime, 1);
        let newPos = new Vec2(this.player.getPixiObj().position.x + change.x, this.player.getPixiObj().position.y + change.y);

        let bounds = ENTITY_BOUNDS;

        if (this.model.map.isSolid(Math.floor(newPos.x - bounds / 2), Math.floor(newPos.y - bounds / 2)) || //Top left
            this.model.map.isSolid(Math.floor(newPos.x + bounds / 2), Math.floor(newPos.y - bounds / 2)) || //Top right
            this.model.map.isSolid(Math.floor(newPos.x + bounds / 2), Math.floor(newPos.y + bounds / 2)) || //Bottom right
            this.model.map.isSolid(Math.floor(newPos.x - bounds / 2), Math.floor(newPos.y + bounds / 2))) { //Bottom left   
            this.sendMessage(MSG_PLAYER_COLLISION);
        }
    }

    /**
    * Checks collison of projectiles with environment.
    * Projectile is small enough that we can approximate it as single point.
    */
    private checkProjectilesCollision() {
        let newProjectiles = Array<PIXICmp.ComponentObject>();

        for (let projectile of this.projectiles) {
            let pos = new Vec2(projectile.getPixiObj().position.x, projectile.getPixiObj().position.y);

            if (this.model.map.isSolid(Math.floor(pos.x), Math.floor(pos.y))) {
                this.sendMessage(MSG_COLLIDABLE_DESTROYED, projectile);
            } else {
                newProjectiles.push(projectile);
            }
        }
        this.projectiles = newProjectiles;
    }

    /**
    * Checks collision using sprite bounds - not very precise
    */
    private detectCollision(first: PIXICmp.ComponentObject, second: PIXICmp.ComponentObject): boolean {
        let boundsA = first.getPixiObj().getBounds();
        let boundsB = second.getPixiObj().getBounds();

        let horIntersect = this.testHorizIntersection(boundsA, boundsB);
        let vertIntersect = this.testVertIntersection(boundsA, boundsB);

        return horIntersect > 0 && vertIntersect > 0;
    }

    /**
    * Checks horizontal intersection
    */
    private testHorizIntersection(boundsA: PIXI.Rectangle, boundsB: PIXI.Rectangle): number {
        return Math.min(boundsA.right, boundsB.right) - Math.max(boundsA.left, boundsB.left);
    }

    /**
     * Checks vertical intersection 
     */
    private testVertIntersection(boundsA: PIXI.Rectangle, boundsB: PIXI.Rectangle): number {
        return Math.min(boundsA.bottom, boundsB.bottom) - Math.max(boundsA.top, boundsB.top);
    }
}